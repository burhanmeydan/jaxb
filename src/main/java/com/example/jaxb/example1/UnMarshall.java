package com.example.jaxb.example1;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class UnMarshall {
    public static  void main(String [] args) throws JAXBException {

        File file =new File("/home/burhan/Desktop/file.xml");
        JAXBContext context=JAXBContext.newInstance(Product.class);

        Unmarshaller jaxbUnmarshaller=context.createUnmarshaller();
        Product product = (Product) jaxbUnmarshaller.unmarshal(file);

        System.out.println("Name : "+ product.name);
        System.out.println("Description : " +product.description);
        System.out.println("Price :"+ product.price);




    }
}
