package com.example.jaxb.example1;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;

public class Marshall {


    public static  void main(String[] args) throws JAXBException {
        // Set Product ;
        Product product=new Product();
        product.setName("shoes");
        product.setDescription("forKids");
        product.setPrice(14.5);

        File file =new File("/home/burhan/Desktop/file.xml");

        JAXBContext jaxbContext=JAXBContext.newInstance(Product.class);
        Marshaller marshaller=jaxbContext.createMarshaller();


        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,true);
        marshaller.marshal(product,file);
        marshaller.marshal(product,System.out);
       }
}
